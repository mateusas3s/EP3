class CreateEmpresas < ActiveRecord::Migration[5.1]
  def change
    create_table :empresas do |t|
      t.string :nome
      t.string :telefone
      t.string :email
      t.string :endereco
      t.text :descricao
      t.string :imagem
      t.int :user_id

      t.timestamps
    end
  end
end
