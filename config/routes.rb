Rails.application.routes.draw do

	#root :to => redirect('/empresas')
	devise_for :users

	resources :empresas

	root :to => "empresas#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
