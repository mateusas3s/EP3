json.extract! empresa, :id, :nome, :telefone, :email, :endereco, :descricao, :imagem, :created_at, :updated_at
json.url empresa_url(empresa, format: :json)
